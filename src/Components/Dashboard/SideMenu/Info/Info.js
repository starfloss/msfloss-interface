import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Star from '../../../Commons/Star/Star.js';
import * as commitService from '../../../../Services/CommitService.js'
import './Info.css';

class Info extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      updated: "",
      members: 0,
      commits: 0
    }
    commitService.getProject(this.props.dataId,
      function(data) {
        this.setState({name: data.data.project_name, updated: data.data.updated_at})
      }.bind(this),
      function(error) {
        console.log(error)
      }
    )

    commitService.getStatistics(this.props.dataId,
      function(data) {
        this.setState({members: data.data.distinct_members, commits: data.data.total_commits})
      }.bind(this),
      function(error) {
        console.log(error)
      }
    )
  }

  render() {
    return (
      <div className={this.props.active ? "side-tab info active" : "side-tab info"}>
        <div className="menu-star-div">
          <Star styleName="menu-star" members={this.state.members} commits={this.state.commits} divId={"menu-star"}/>
          <p className="name">{this.state.name}</p>
        </div>
        <div className="data">
          <p>Commiters: {this.state.members}</p>
          <p>Commits: {this.state.commits}</p>
          <p>Last updated: {new Date(this.state.updated).toString()}</p>
        </div>
      </div>
    )
  }
}

Info.propTypes = {
  active: PropTypes.bool,
  dataId: PropTypes.string
}

export default Info
