import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3v4'
import * as d3Sankey from 'd3-sankey'
import * as d3Color from 'd3-scale-chromatic'
import './RelationChart.css'

class RelationChart extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    var parseDate = d3.timeParse("%Y-%m-%dT%H:%M:%S%Z")
    var data = this.props.data.data
    console.log(data)
    data = d3.nest()
      .key(function(d) { return d.author.email })
      .key(function(d) { return d.commiter.email })
      // .rollup(function(d) { console.log(d); return d.length })
      .entries(data)
    console.log(data)
    var graph = {"nodes" : [], "links" : []};

    data.forEach(function (source) {
      graph.nodes.push({ "name": source.key })
      source.values.forEach(function (target){
        graph.nodes.push({ "name": target.key })
        graph.links.push({ "source": source.key,
                           "target": target.key,
                           "value": +target.values.length });
      })
    })

    graph.nodes = d3.keys(d3.nest()
      .key(function (d) { return d.name; })
      .object(graph.nodes));

    graph.links.forEach(function (d, i) {
      graph.links[i].source = graph.nodes.indexOf(graph.links[i].source);
      graph.links[i].target = graph.nodes.indexOf(graph.links[i].target);
    });

    graph.nodes.forEach(function (d, i) {
      graph.nodes[i] = { "name": d };
    });

    console.log(graph)
    this.drawRelationChart(graph)
  }

  drawRelationChart(data) {
    // const max = d3.max(data)
    const color = d3.scaleOrdinal(d3Color.schemeBlues[20]);
    const margin = {top: 30, left: 50, down: 30, right: 50}
    const height = 300 + margin.top + margin.down
    const width = 500 + margin.left + margin.right
    const nodeWidth = 30
    const nodePadding = 50

  }

  render() {
    return (
      <div className="relation-chart">
      </div>
    )
  }
}

RelationChart.propTypes = {
  data: PropTypes.object,
  id: PropTypes.string
}

export default RelationChart
