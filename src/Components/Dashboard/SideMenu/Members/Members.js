import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Star from '../../../Commons/Star/Star.js';
import constants from '../../../../Utils/Constants.js';
import * as d3 from 'd3'
import * as commitService from '../../../../Services/CommitService.js'
import './Members.css';

class Members extends Component {
  constructor(props) {
    super(props)
    this.state = {active: "none",
                  members: [],
                  data: [],
                  shownData: [],
                  name: "All members"}

    commitService.getDiffs(this.props.dataId,
      function(data) {
        var pd = this.processData(data.data, "name", "commits")
        this.setState({data: pd, name: "All members"})
      }.bind(this),
      function(error) {
        console.log(error)
      }
    )

    this.props.dispatch.on("heatmap.side", function(params){
      if (params.reset) {
        this.setState({name: "All members", shownData: this.state.data})
        return
      }

      var message = ""
      for (var i = 0; i < params.dates.length; i++) {
        var hour = params.dates[i].hour.split("hours")[1]
        var day = params.dates[i].week.split("weekDays")[1]
        message = message + constants.weekday[day] + " at " + constants.dayHours[hour] + ", "
      }
      this.setState({name: "Commits on specific dates", filters: message.slice(0, -2)})
      var data = params.data.flat()
      data = d3.nest()
        .key(function(d) { return d.author_email })
        .entries(data)
      this.processData(data, "key", "values")
      this.props.setActive("members")
    }.bind(this))

    this.eachMember = this.eachMember.bind(this)
    this.processData = this.processData.bind(this)
    this.setActive = this.setActive.bind(this)
  }

  processData(data, email, commits) {
    var processedData = []
    for (var i = 0; i < data.length; i++) {
      processedData[i] = {email: data[i][email].split("@"),
                          commits: data[i][commits].length,
                          commits_dates: data[i][commits].date}
    }
    processedData = processedData.sort(function(a, b){ return - (a.commits - b.commits) })
    this.setState({shownData: processedData})
    return processedData
  }

  setActive(id) {
    if (this.state.active === id) {
      this.props.dispatch.call("members", this, {data: id, reset: true})
      this.setState({active: "none"})
    } else {
      this.props.dispatch.call("members", this, {data: id, reset: false})
      this.setState({active: id})
    }
  }

  eachMember(member) {
    return (
      <div className={(this.state.active === member.email[0] + "@" + member.email[1]) ? "member-div active" : "member-div"}
           key={member.email[0] + "@" + member.email[1]}
           onClick={this.setActive.bind(this, member.email[0] + "@" + member.email[1])}>
        <p>{member.email[0]}<br></br>@{member.email[1]}</p>
        <div className="commit-div">
          <p>COMMITS</p>
          <p>{member.commits}</p>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className={this.props.active ? "side-tab members active" : "side-tab members"}>
        <h2>{this.state.name}</h2>
        <p style={{marginTop: 0}}>{this.state.filters}</p>
        <div className="members-div">
          {this.state.shownData.map(this.eachMember)}
        </div>
        <div className="stacked-chart">
        </div>
        <div className="bar-chart">
        </div>
      </div>
    )
  }
}

Members.propTypes = {
  active: PropTypes.bool,
  setActive: PropTypes.func,
  dataId: PropTypes.string,
  dispatch: PropTypes.object,
}

export default Members
