import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";
import './index.css';
import Home from './Components/Home/Home.js';
import Dashboard from './Components/Dashboard/Dashboard.js';
import Infograph from './Components/Infograph/Infograph.js';
import {Store} from './Store/Store.js'
import { Provider } from 'react-redux'

ReactDOM.render((
  <BrowserRouter>
    <Provider store={Store}>
      <Route path="/" exact component={Home} />
    </Provider>
    <Route path="/dashboard/:id" component={Dashboard} />
    <Route path="/infograph" component={Infograph} />
  </BrowserRouter>
), document.getElementById('root'));
