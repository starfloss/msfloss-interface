import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3v4'

import './Star.css';

class Star extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    var members = this.props.members
    var commits = this.props.commits
    this.drawStar(members, commits)
  }

  componentDidUpdate() {
    var members = this.props.members
    var commits = this.props.commits
    this.drawStar(members, commits)
  }

  componentWillReceiveProps() {
    d3.select("." + this.props.divId).selectAll("svg").remove()
    d3.select("." + this.props.divId).selectAll("div").remove()
  }

  drawStar(members, commits) {
    var rScale = d3.scaleLinear()
                .domain([0, 3000])
                .range([2,30]);
    var pulseScale = d3.scaleLinear()
                .domain([0, 60000])
                .range([6,90]);

    const star = d3.select("." + this.props.divId).append("svg").attr("viewBox", "0 0 60 60").attr("id", this.props.divId)

    star.selectAll("circle")
      .data([members])
      .enter()
        .append("circle")
        .attr("class", "circle")
        .attr("fill", "#ffffff")
        .attr("cx", 30)
        .attr("cy", 30)
        .attr("r", function(d){ return rScale(d) })

    d3.select("." + this.props.divId).append("div")
    .attr("class", this.props.divId + "-pulse pulse")

    const pulse = d3.select("." + this.props.divId + "-pulse").append("svg").attr("viewBox", "0 0 180 180")
    pulse.selectAll("circle")
      .data([commits])
      .enter()
        .append("circle")
        .attr("class", "circle")
        .attr("fill", "#ffffff")
        .attr("cx", 90)
        .attr("cy", 90)
        .attr("r", function(d){ return pulseScale(d) })
}

  render() {
    return (
      <div className={"star " + this.props.divId + " " + this.props.divClasses}>

      </div>
    )
  }
}

Star.propTypes = {
  members: PropTypes.number,
  commits: PropTypes.number,
  lastCommit: PropTypes.string,
  divId: PropTypes.string,
  divClasses: PropTypes.string
}

export default Star
