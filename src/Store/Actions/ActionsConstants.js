const constants = {
  ADD_PROJECT: "ADD_PROJECT",
  ADD_STATISTIC: "ADD_STATISTIC",
  REMOVE_PROJECT: "REMOVE_PROJECT",
}

export default constants
