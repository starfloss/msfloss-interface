import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SideMenu from './SideMenu/SideMenu.js';
import MainMenu from './MainMenu/MainMenu.js';
import * as d3 from 'd3'
import './Dashboard.css';

class Dashboard extends Component {
  constructor(props) {
    super(props)
    // var dispatch = d3.dispatch("console");
    this.state = {dispatch: d3.dispatch("heatmap", "bubble", "members")}
  }

  render() {
    return (
      <div className="dashboard">
        <SideMenu dataId={this.props.match.params.id} dispatch={this.state.dispatch}/>
        <MainMenu dataId={this.props.match.params.id} dispatch={this.state.dispatch}/>
      </div>
    )
  }
}

export default Dashboard
