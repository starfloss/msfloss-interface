import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Star from '../../Commons/Star/Star.js';
import Info from './Info/Info.js';
import Members from './Members/Members.js';
import * as commitService from '../../../Services/CommitService.js'
import './SideMenu.css';

class SideMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {active: "info"}

    this.setActive = this.setActive.bind(this)
  }

  setActive(id) {
    this.setState({active: id})
  }

  render() {
    return (
      <div className="sidemenu">
        <div className="tabs">
          <p onClick={this.setActive.bind(this, "info")}>Info</p>
          <p onClick={this.setActive.bind(this, "members")}>Members</p>
        </div>
        <Info active={(this.state.active === "info")} dataId={this.props.dataId}/>
        <Members setActive={this.setActive} active={(this.state.active === "members")} dataId={this.props.dataId} dispatch={this.props.dispatch}/>
      </div>
    )
  }
}

SideMenu.propTypes = {
  dataId: PropTypes.string,
  dispatch: PropTypes.object
}

export default SideMenu
