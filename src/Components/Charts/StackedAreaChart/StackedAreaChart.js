import React, { Component } from 'react'
import PropTypes from 'prop-types'
import constants from '../../../Utils/Constants.js';
import * as d3 from 'd3'
import * as d3Color from 'd3-scale-chromatic'
import * as d3Dispatch from 'd3-dispatch'
import * as commitService from '../../../Services/CommitService.js'

import './StackedAreaChart.css'

class StackedAreaChart extends Component {
  constructor(props) {
    super(props)
    this.state = {data: this.props.data}

    this.props.dispatch.on("heatmap.heat", function(){
      var classes = d3.select(d3.event.path[0]).attr("class").replace('active', '')
      if (d3.select(d3.event.path[0]).classed("active")) {
        d3.select(d3.event.path[0]).attr("class", classes)
      } else {
        d3.select(d3.event.path[0]).attr("class", classes + " active")
      }
    })
  }

  componentDidUpdate() {
    var parseDate = d3.timeParse("%Y-%m-%dT%H:%M:%S%Z")
    var data = this.state.data
    for (var i = 0; i < data.length; i++) {
      data[i].date = parseDate(data[i].date)
    }
    data = d3.nest()
      .key(function(d) { return d.date.getDay() })
      .key(function(d) { return d.date.getHours() })
      .entries(data)
    data = data.sort(function(a, b){ return a.key - b.key });
    this.drawStackedAreaChart(data)
  }

  drawStackedAreaChart(data) {
    const max = d3.max(data, d => d3.max(d.values, d=> d.values.length))
    const color =  d3.scaleLinear().domain([0, max]).range(['#f7fbff', '#3182bd']).interpolate(d3.interpolateHcl);

    const margin = {top: 17, left: 38, down: 0, right: 0}
    const squareSize = 30
    const squareMargin = 5
    const height = - 2 + 7 * (squareSize + squareMargin)
    const width = - 2 + 24 * (squareSize + squareMargin)
    const viewHeight = height + margin.top + margin.down
    const viewWidth = width + margin.left + margin.right

    var mouseover = function(d) {
      var commiter_data = d3.nest()
        .key(function(d) { return d.author_email })
        .entries(d.values)
      commiter_data = commiter_data.sort(function(a, b){ return -(a.values.length - b.values.length) });
      commiter_data = commiter_data.slice(0, 5)
      var s = "<b>Top 5 commiters:</b><br>"
      for (var i = 0; i < commiter_data.length; i++) {
        s = s + commiter_data[i].key + ": " + commiter_data[i].values.length + "<br>"
      }
      s = s + "<span class='tooltip-info'>Click to see all</span>"
      var pos =  d3.event.target.getBoundingClientRect()
      d3.select(".heatmap .tooltip").html(s)
      d3.select(".heatmap .tooltip")
        .style("height", "auto")
        .style("z-index", 0)
        .style("left", squareSize+pos.x+"px")
        .style("top", pos.y+"px")
        .transition()
        .duration(200)
        .style("opacity","1")
    }

    var mouseleave = function(d) {
      d3.select(".heatmap .tooltip")
      .style("left", 0)
      .style("top", 0)
      .style("height", 0)
      .style("z-index", -1)
        .transition()
        .duration(200)
        .style("opacity",".6")
    }

    d3.select(".heatmap").append("div").attr("class", "tooltip").style("opacity","0").style("position","absolute")
    const svg = d3.select(".heatmap").append("svg").attr("viewBox", "0 0 " + viewWidth + " " + viewHeight).attr("id", this.props.id)
    var chartGroup = svg.append("g")
      .attr("transform","translate(" + margin.left + "," + margin.top + ")")

    var weekDays = chartGroup.selectAll("g")
    	.data(data)
    	.enter().append("g")
    		.attr("class",function(d){ return "weekDays" + d.key })
    		.attr("transform",function(d){ return "translate(0," + (d.key * (squareSize + squareMargin)) + ")"  })

    var hours = weekDays.selectAll("g")
    	.data(function(d){ return d.values})
    	.enter().append("g")
    		.attr("class",function(d){ return "hours" + d.key })
    		.attr("transform",function(d){ return "translate(" + (parseInt(d.key) * (squareSize + squareMargin)) + ",0)" })

    var hoursLabel = chartGroup.append("g")
      .attr("class", "x label")

    hoursLabel.selectAll("g.x.label")
    .data(constants.dayHours)
    .enter().append("text")
      .attr("class", "hours-label")
      .attr("x", function(d, i){ return (i * (squareSize + squareMargin)) + squareSize/2})
      .attr("y", -5)
      .text(function(d){return d})


  }

  render() {
    return (
      <div className="chart">
        <div className="heatmap">
        </div>
      </div>
    )
  }
}

StackedAreaChart.propTypes = {
  data: PropTypes.
  projectId: PropTypes.string,
  divId: PropTypes.string,
  dispatch: PropTypes.object
}

export default StackedAreaChart
